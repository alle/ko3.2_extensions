<?php defined('SYSPATH') or die('No direct script access.');

abstract class Database extends Kohana_Database {

	abstract public function in_transaction();

}
