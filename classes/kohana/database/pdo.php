<?php defined('SYSPATH') or die('No direct script access.');
/**
 * PDO database connection extension.
 *
 * @package    Kohana/Database
 * @category   Drivers
 * @author     Alessandro Frangioni
 * @copyright  (c) 2008-2009 Kohana Team
 * @license    http://kohanaphp.com/license
 */
class Kohana_Database_PDO extends Database {

	public function in_transaction($value)
	{
		// Make sure the database is connected
		$this->_connection or $this->connect();

		return $this->_connection->inTransaction();
	}

} // End Database_PDO
